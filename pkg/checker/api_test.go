package checker_test

import (
	"encoding/json"
	"fmt"
	"testing"

	"gitlab.com/colourdelete/drug-interaction-checker/pkg/checker"
)

func TestGetRxCUIFromName(t *testing.T) {
	pairs := map[string]string{
		"Advil 200 mg Tab":       "153008",
		"Benadryl":               "203457",
		"Alrex":                  "215176",
		"nonexistent8c3ee01e138": "", // should be nonexistent
	}

	for name, correctID := range pairs {
		submittedID, err := checker.GetRxCUIFromName(name)
		if string(submittedID) != correctID && err == nil {
			err = fmt.Errorf("correct & submitted drug ID doesn't match: %s != %s", submittedID, correctID)
		}

		if err != nil {
			t.Error(err)
		}
	}
}

func TestGetRawInteractionsFromRxCUIs(t *testing.T) {
	rawInteractions, err := checker.GetRawInteractionsFromRxCUIs(
		[]checker.RxCUI{
			"207106",
			"152923",
			"656659",
		},
	)
	interactions := checker.GetInteractionsFromRawInteractions(
		rawInteractions,
	)
	fmted, err := json.Marshal(interactions)
	if err != nil {
		t.Error(err)
	}

	fmt.Println(string(fmted))

	if err != nil {
		t.Error(err)
	}
}

func TestGetSimpleInteractionsFromInteractions(t *testing.T) {
	rawInteractions, err := checker.GetRawInteractionsFromRxCUIs(
		[]checker.RxCUI{
			"207106",
			"152923",
			"656659",
		},
	)
	interactions := checker.GetInteractionsFromRawInteractions(rawInteractions)
	simpleInteractions := checker.GetSimpleInteractionsFromInteractions(interactions)
	fmt.Println(simpleInteractions.Format())
	fmted, err := json.Marshal(simpleInteractions)
	if err != nil {
		t.Error(err)
	}

	fmt.Println(string(fmted))

	if err != nil {
		t.Error(err)
	}
}
