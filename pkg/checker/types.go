package checker

import "fmt"

type RxCUIFromNameResponse struct {
	IDGroup struct {
		Name   string   `json:"name"`
		RxCUIs []string `json:"rxnormId"`
	} `json:"idGroup"`
}

type RxCUI string

type GetRawInteractionsFromRxCUIsOptions struct {
	BaseURL string
}

var GetRawInteractionsFromRxCUIsDefaultOptions = GetRawInteractionsFromRxCUIsOptions{
	BaseURL: "https://rxnav.nlm.nih.gov/REST/interaction/list.json?rxcuis=%s",
}

type Drug struct {
	RxCUI RxCUI
	Name  string
	Tty   string
	ID    string
	URL   string
}

type Interactions struct {
	Disclaimer   string
	Interactions []Interaction
}

type Interaction struct {
	Source  Source
	Comment string
	Group   Group
}

type Source struct {
	Name       string
	Disclaimer string
}

type Group struct {
	Drugs    []Drug
	Severity string
	Comment  string
}

type RawInteractions struct {
	NlmDisclaimer string `json:"nlmDisclaimer"`
	UserInput     struct {
		Sources []string `json:"sources"`
		RxCUIs  []RxCUI  `json:"rxcuis"`
	} `json:"userInput"`
	FullInteractionTypeGroup []struct {
		SourceDisclaimer    string `json:"sourceDisclaimer"`
		SourceName          string `json:"sourceName"`
		FullInteractionType []struct {
			Comment    string `json:"comment"`
			MinConcept []struct {
				RxCUI RxCUI  `json:"rxcui"`
				Name  string `json:"name"`
				Tty   string `json:"tty"`
			} `json:"minConcept"`
			InteractionPair []struct {
				InteractionConcept InteractionConcept `json:"interactionConcept"`
				Severity           string             `json:"severity"`
				Description        string             `json:"description"`
			} `json:"interactionPair"`
		} `json:"fullInteractionType"`
	} `json:"fullInteractionTypeGroup"`
}

type InteractionConcept []struct {
	MinConceptItem struct {
		RxCUI RxCUI  `json:"rxcui"`
		Name  string `json:"name"`
		Tty   string `json:"tty"`
	} `json:"minConceptItem"`
	SourceConceptItem struct {
		ID   string `json:"id"`
		Name string `json:"name"`
		URL  string `json:"url"`
	} `json:"sourceConceptItem"`
}

type SimpleInteractions []SimpleInteraction

type SimpleInteraction struct {
	Drugs       []string
	Description string
	Source      string
}

func (s *SimpleInteractions) Format() string {
	dest := ""
	for _, inter := range *s {
		dest += inter.Format()
	}
	return dest
}

func (s *SimpleInteraction) Format() string {
	var dest string
	dest += "START=====\n"
	dest += "Drugs:\n"
	for i, drug := range s.Drugs {
		dest += fmt.Sprintf("%v. %s\n", i+1, drug)
	}
	dest += "\n"
	dest += "Description:\n"
	dest += s.Description + "\n"
	dest += "END=======\n"
	return dest
}
