package main

import "gitlab.com/colourdelete/drug-interaction-checker/internals/drugCheck"

func main() {
	drugCheck.Run()
}
